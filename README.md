# rustpp_enum

A simple small crate which will generate 
common Rust and C++ integers
by reading a toml file

I have a usecase in multiple projects where I find myself sending integers over wire between
rust server and qt clients. This crate is for that usecase.

## how to use

checkout test mod in src/lib.rs at the bottom of file
