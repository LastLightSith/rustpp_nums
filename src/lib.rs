#[macro_use]
extern crate fstrings;

pub fn rustfmt(filepath : &str) {
    let out = std::process::Command::new("rustfmt")
        .arg(filepath)
        .output()
        .expect("failed to run rustfmt");

    if !out.stderr.is_empty() {
        panic!("rustfmt error: {}", String::from_utf8(out.stderr).unwrap())
    }
}

pub fn clang_format(filepath : &str) {
    let out = std::process::Command::new("clang-format")
        .arg("-i")
        .arg(filepath)
        .output()
        .expect("failed to run clang-format");

    if !out.stderr.is_empty() {
        panic!("clang-format error: {}", String::from_utf8(out.stderr).unwrap())
    }
}

fn get_rs_type(size: usize) -> &'static str {
    if size <= u8::MAX as usize {
        "u8"
    } else if size <= u16::MAX as usize {
        "u16"
    } else if size <= u32::MAX as usize {
        "u32"
    } else {
        "u64"
    }
}

fn get_cpp_type(size: usize) -> &'static str {
    if size <= u8::MAX as usize {
        "std::uint8_t"
    } else if size <= u16::MAX as usize {
        "std::uint16_t"
    } else if size <= u32::MAX as usize {
        "std::uint32_t"
    } else {
        "std::uint64_t"
    }
}

pub struct CommonNumbers {
    map: std::collections::HashMap<String, Vec<String>>,
}

impl CommonNumbers {

    pub fn from_file(path: &str) -> Self {
        let data = std::fs::read(path).unwrap_or_else(|_| panic_f!("can't read from {path}"));
        CommonNumbers {
            map: toml::de::from_slice(&data).unwrap(),
        }
    }

    pub fn gen_rust(&self) -> String {
        let mut c = String::new();

        for (name, array) in &self.map {

            let typ = get_rs_type(array.len());

            c += &f!("pub mod {name} {{");
            c += &f!("pub type Type = {typ};");

            // types
            for (index, name) in array.iter().enumerate() {
                c += &f!("pub const {name} : {typ} = {index};");
            }

            // fn to_string
            c += &f!("pub fn to_string(num : {typ}) -> &'static str {{
                match num {{ ");

            for (index, name) in array.iter().enumerate() {
                c += &f!("{index} => \"{name}\",");
            }

            c += &f!("other => \"Unknown_{name}\",");

            c += "}"; // match end
            c += "}"; // function end
            c += "}"; //mod end
        }
        c
    }

    pub fn gen_cpp(&self) -> String {
        let mut c = String::from("#include <cstdint>\n");

        for (name, array) in &self.map {

            let typ = get_cpp_type(array.len());

            c += &f!("namespace {name} {{") ;
            c += &f!("typedef {typ} {name};");

            // inlining function temporarily until C++20 module feature is implemented in cmake and gcc

            // types
            for (index, name) in array.iter().enumerate() {
                c += &f!("inline constexpr {typ} {name} = {index};");
            }

            // fn to_string
            c += &f!("inline const char* to_string({typ} num ) {{
                switch (num) {{ ");

            for (index, name) in array.iter().enumerate() {
                c += &f!("case {index}: return \"{name}\";");
            }

            c += &f!("default: return \"Unknown_{name}\";");

            c += "}"; // switch end
            c += "}"; // function end
            c += "};\n\n"; //namespace end
        }
        c
    }
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    pub fn test() {
        let ce = CommonNumbers::from_file("sample.toml");

        let cpp = ce.gen_cpp();
        let rs = ce.gen_rust();

        std::fs::write("test.rs", rs).unwrap();
        rustfmt("test.rs");

        std::fs::write("test.cpp", cpp).unwrap();
        clang_format("test.cpp")
    }
}